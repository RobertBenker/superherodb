# Create a Database and access it assignment

Here you will find our 2nd .NET assignment consisting of:
  * SQL scripts to create the SuperheroDb
  * c# console application to demonstrate how to access SQL Database.

all based on the requirements in the accompanying PDF

## Prerequisites / Dependencies

  * Visual Studio 2019 or higher with .NET 5.0
  * SQL Server Instance
  * SQL Server Management Studio (SSMS)
  * Import Chinook_SqlServer_AutoIncrementPKs.sql to your SQL Server
  * Your Windows user account need full access to the Chinook Database.

## Getting Started

SQL Script Part
  - import the .sql files in the SQL_Script folder in order from 01 - 09
  - both files 98 and 99 demonstrate some additional things like creating a linking table and creating a join query for clarification
  - the function of the files 01 - 09 are explained in the assignment accompanying PDF
  - to illustrate the structure and the linking of the data tables in the SuperheroDB after importing the .sql files, please also look at the database diagram (00_SuperheroDb_DatabaseDiagram.png)

C# .NET Console App
  - Clone to a local directory.
  - Open solution in Visual Studio
  - Run the application

### Usage of console application

It was necessary to program our console application in regards to customer specific requirements.
In total we had to implement 9 different requirements.

The Application expected access to a local Microsoft SQL Database Server with Chinook Database and access by 
windows Credentials.

Run our console application and select the apropriate customer requirement as you like.
The customer requirements 1-9 from appendix B are matching within our user selection enumeration (menu). 
Choose any option you like in any order.


```
===========================================================================================

  Welcome to the user menu !

===========================================================================================
    1. Show all customers from our database
    2. Show a specific customer from our database by Id (11)
    3. Show a specific customer from our database by Name (Victor Stevens)
    4. Show all customers data page by page
    5. Add a new customer to the database
    6. Update an existing customer
    7. Show the ammount of our customers in each country (ordered high -> low)
    8. Show customers who are the highest spenders (ordered high -> low)
    9. Show the most popular genre for given customers 8 & 56 to show the differences
    0. Exit application

===========================================================================================
  Please select one of the provided options:

===========================================================================================
```


## Authors

***Robert Benker** [Robert.Benker@de.experis.com]
***Matthias Friedrich** [Matthias.Friedrich@de.experis.com]
***Thomas Bauer** [Thomas.Bauer@de.experis.com]
