-- delete one assistant by using DELETE, filtered to one assistant with WHERE

DELETE FROM Assistant 
	WHERE Assistant.Name = 'Otto';