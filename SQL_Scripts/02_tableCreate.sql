USE SuperheroesDb;

CREATE TABLE Superhero (
    Id int IDENTITY(1,1),
    Name varchar(255) NOT NULL,
    Alias varchar(255) NOT NULL,
    Origin varchar(255) NULL DEFAULT 'unknown'
);

CREATE TABLE Assistant (
    Id int IDENTITY(1,1),
    Name varchar(255) NOT NULL
);

CREATE TABLE Power (
    Id int IDENTITY(1,1),
    Name varchar(255) NOT NULL,
	Description varchar(255) NULL DEFAULT 'unknown'
);

-- Setting up the Primary Keys for all those upper already created tables to show how ALTER TABLE works

ALTER TABLE Superhero
	ADD PRIMARY KEY (Id);

ALTER TABLE Assistant
    ADD PRIMARY KEY (Id);

ALTER TABLE Power
	ADD PRIMARY KEY (Id);