-- creating some entities to the Linking Table to show how it works

INSERT INTO "SuperheroesDb"."dbo"."LinkingPowerToHero" ("SuperheroID", "PowerID") 
	VALUES (1, 2),
	(1, 3),
	(1, 4),
	(1, 5),
	(12, 1),
	(3, 2),
	(5, 2),
	(6, 2);

