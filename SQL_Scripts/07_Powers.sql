-- insert some entities for some Super Hero Power

INSERT INTO "SuperheroesDb"."dbo"."Power" ("Name", "Description") 
	VALUES ('Quaken', 'can trigger an earthquake'),
	('Heatresitance', 'Heatresitance'),
	('Fly', 'is able to Fly like a Bird'),
	('Laser', 'Can generate Laserbeams from its own'),
	('Superstrength', 'is at minimum as twice as strong as a normal Human');