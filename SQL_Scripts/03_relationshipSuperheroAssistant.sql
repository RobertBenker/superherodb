-- Add additional Column to SuperheroDb and create a foreign key as a reference between Superhero.Id and Assistant.SuperheroId (NEW)

Use SuperheroesDb;

ALTER TABLE Assistant
ADD SuperheroID int NOT NULL,
    FOREIGN KEY (SuperheroID) REFERENCES Superhero(Id);