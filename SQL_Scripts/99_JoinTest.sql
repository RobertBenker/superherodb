-- here are two SELECT commands with INNER JOIN as an example to show how the data can be linked and filtered

SELECT Superhero.Alias Superhero, 'to' Connection, power.Name Superpower FROM Superhero 
	INNER JOIN LinkingPowerToHero ON Superhero.Id = LinkingPowerToHero.SuperheroID
	INNER JOIN Power ON LinkingPowerToHero.PowerID = power.Id WHERE Superhero.Alias = 'Superman' order by power.Name;


SELECT power.Name Superpower, 'to' Connection, Superhero.Alias Superhero  FROM Superhero 
	INNER JOIN LinkingPowerToHero ON Superhero.Id = LinkingPowerToHero.SuperheroID
	INNER JOIN Power ON LinkingPowerToHero.PowerID = power.Id WHERE power.Name = 'Heatresitance' order by Power.Name;