-- creating some Superheros, one of those has no origin to check how the default value works

INSERT INTO "SuperheroesDb"."dbo"."Superhero" ("Name", "Alias", "Origin") 
	VALUES ('Clark Kent', 'Superboy', 'Krypton'),
	('Bruce Wayne', 'Batman', 'Earth'),
	('Thor', 'Thor', 'Asgard'),
	('Captain Future', 'Captain Future', 'Earth'),
	('Tony Stark', 'Ironman', 'Earth'),
	('Bruce Banner', 'HULK', 'Earth'),
	('Natasha Romanova', 'Black Widow', 'Earth'),
	('Steve Rogers', 'Captain America', 'Earth'),
	('Logan', 'Wolverine', 'Earth'),
	('Hal Jordan', 'Green Lantern', 'Earth'),
	('Arthur Curry', 'Aquaman', 'Earth'),
	('Daisy Johnson', 'Quake', 'Earth'),
	('Matthias Friedrich', 'KnowItAll', 'Moon'),
	('Thomas Bauer', 'Thinker', 'Mars');

INSERT INTO "SuperheroesDb"."dbo"."Superhero" ("Name", "Alias") VALUES ('Robert Benker', 'Smartass');