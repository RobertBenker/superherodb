-- update in Superhero Table Alias from superboy to superman

UPDATE Superhero
	SET Alias = 'Superman'
	WHERE Alias = 'Superboy';