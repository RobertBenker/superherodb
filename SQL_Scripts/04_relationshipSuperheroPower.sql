-- Creating a linking Table to match Superheros to powers (N:N) afterwards add the Primary and the needed Foreign keys to this Table
-- by using ALTER TABLE
Use SuperheroesDb;

CREATE TABLE LinkingPowerToHero (
    Id int IDENTITY(1,1),
    SuperheroID int NOT NULL,
	PowerID int NOT NULL
);

ALTER TABLE LinkingPowerToHero
	ADD PRIMARY KEY (Id),
	FOREIGN KEY (PowerID) REFERENCES Power(Id),
	FOREIGN KEY (SuperheroID) REFERENCES  Superhero(Id);