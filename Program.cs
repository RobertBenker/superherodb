﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace dotnetcore
{
    class Program
    {
        /// <summary>
        /// Instantiating our SQL Client object
        /// </summary>
        /// 
        ClsMSSQL MSSQL = new ClsMSSQL();
        ClsMSSQL.DBCredentials DBCR = new ClsMSSQL.DBCredentials();
        int HeaderPadding = Console.LargestWindowWidth / 100 * 99;


        // Customer Requirements #1
        public void GetCustomerFromDB()
        {            
            try
            {   // we use the following SQL Statement to generate the required data and output as an example
                DBCR.SQLString = @"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email 
                       FROM Customer";
                DBCR.Table = "Customer";
                DBCR.Remark = "       Used statement: " + DBCR.SQLString;
                GenerateOutput(DBCR, MSSQL.GetHeaderToList(DBCR), MSSQL.GetDataToList(DBCR),2);
                // 
                Console.WriteLine();
                Console.WriteLine("".PadRight(HeaderPadding, '='));
                Console.WriteLine();
                Console.WriteLine("Press <ENTER> to get back to the main menu");
                Console.WriteLine();
                Console.WriteLine("".PadRight(HeaderPadding, '='));
                Console.ReadLine();

                CreateUserMenu();
            }
            catch (Exception EX)
            {
                Console.WriteLine(EX.Message);
            }

        }


        // Customer Requirements #2 & #3
        public void GetSpecificCustomerFromDB(string strgWhere)
        {            
            try
            {   // we use the following SQL Statement to generate the required data and output as an example
                DBCR.SQLString = @"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email 
                       FROM Customer " + strgWhere;
                DBCR.Table = "Customer";
                DBCR.Remark = "       Used statement: " + DBCR.SQLString;
                GenerateOutput(DBCR, MSSQL.GetHeaderToList(DBCR), MSSQL.GetDataToList(DBCR), 2);
                Console.WriteLine();
                Console.WriteLine("".PadRight(HeaderPadding, '='));
                Console.WriteLine();
                Console.WriteLine("Press <ENTER> to get back to the main menu");
                Console.WriteLine();
                Console.WriteLine("".PadRight(HeaderPadding, '='));
                Console.ReadLine();

                CreateUserMenu();
            }
            catch (Exception EX)
            {
                Console.WriteLine(EX.Message);
            }

        }


        // Customer Requirements #4
        public void GetCustomerFromDBbyPage(int offset, int fetchNext)
        {            
            try
            {   // we use the following SQL Statement to generate the required data and output as an example
                DBCR.SQLString = @$"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email 
                       FROM Customer 
                       ORDER BY CustomerId 
                       OFFSET {offset} ROWS FETCH NEXT {fetchNext} ROWS ONLY";
                DBCR.Table = "Customer";
                DBCR.Remark = "       Used statement: " + DBCR.SQLString;
                GenerateOutput(DBCR, MSSQL.GetHeaderToList(DBCR), MSSQL.GetDataToList(DBCR), 2);
                Console.WriteLine();
                Console.WriteLine("".PadRight(HeaderPadding, '='));
                Console.WriteLine();
                Console.WriteLine("Press <SPACE> to jump to the next page or press <ENTER> to get back to the main menu");
                Console.WriteLine();
                Console.WriteLine("".PadRight(HeaderPadding, '='));    

                ConsoleKeyInfo input;
                input = Console.ReadKey();

                Console.WriteLine(input.Key.ToString()+":");
                
                if (input.Key.ToString() == "Spacebar")
                {
                    offset += fetchNext;
                    GetCustomerFromDBbyPage(offset, fetchNext);
                }
                else
                {
                    CreateUserMenu();
                }
                
            }
            catch (Exception EX)
            {
                Console.WriteLine(EX.Message);
            }

        }


        // Customer Requirements #5
        public void AddNewCustomer()
        {            
            try
            {   // we use the following SQL Statement to generate the required data and output as an example
                DBCR.SQLString = @"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email
                       FROM Customer 
                       WHERE FirstName = 'Jon' AND LastName = 'Doe';";
                DBCR.Table = "Customer";
                DBCR.Remark = @"  Firstly we check if Jon Doe exists 
       Used statement: " + DBCR.SQLString;
                GenerateOutput(DBCR, MSSQL.GetHeaderToList(DBCR), MSSQL.GetDataToList(DBCR), 2);
                // 
                Console.WriteLine();
                Console.WriteLine("".PadRight(HeaderPadding, '='));
                Console.WriteLine();
                Console.WriteLine("Press <ENTER> to create a dataset for Jon Doe!");
                Console.WriteLine();
                Console.WriteLine("".PadRight(HeaderPadding, '='));
                Console.ReadLine();

                DBCR.SQLString = @"INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email) 
                        VALUES('Jon', 'Doe', 'USA', '94044-1234', '+1 (555) 375-2964', 'jon.doe@anywhere.us'); ";
                if (MSSQL.SendSQLCommand(DBCR))
                {
                    Console.WriteLine("Jon Doe dataset was successfully created!");
                }
                else
                {
                    Console.WriteLine("command not executed successfully");
                    if (MSSQL.boError)
                    {
                        Console.WriteLine("Please ask your Support for help and please provide the following information");
                        Console.WriteLine("");
                        Console.WriteLine($"   Error Number: <{MSSQL.intErrorNumber}>, error Description: <{MSSQL.strgErrorDescription}>");
                    }
                }
                Console.WriteLine("");
                Console.WriteLine("Press <ENTER> to to view the data just created!");
                Console.ReadLine();
                DBCR.SQLString = @"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email
                       FROM Customer 
                       WHERE FirstName = 'Jon' AND LastName = 'Doe';";
                DBCR.Table = "Customer";
                DBCR.Remark = @"  Firstly we check if Jon Doe exists 
  The output based on: " + DBCR.SQLString;
                GenerateOutput(DBCR, MSSQL.GetHeaderToList(DBCR), MSSQL.GetDataToList(DBCR), 2,true);
                // 
                Console.WriteLine();
                Console.WriteLine("".PadRight(HeaderPadding, '='));
                Console.WriteLine();
                Console.WriteLine("Press <ENTER> to get back to the main menu");
                Console.WriteLine();
                Console.WriteLine("".PadRight(HeaderPadding, '='));
                Console.ReadLine();
                CreateUserMenu();
            }
            catch (Exception EX)
            {
                Console.WriteLine(EX.Message);
            }

        }


        // Update Existing customer #6
        public void UpdateExistingCustomer()
        {            
            try
            {   // we use the following SQL Statement to generate the required data and output as an example
                DBCR.SQLString = @"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email
                       FROM Customer
                       WHERE FirstName = 'Jon'
                       AND LastName = 'Doe';";
                DBCR.Table = "Customer";
                DBCR.Remark = @"  Firstly we check if Jon Doe exists 
       Used statement: " + DBCR.SQLString;
                GenerateOutput(DBCR, MSSQL.GetHeaderToList(DBCR), MSSQL.GetDataToList(DBCR), 2);


                if (MSSQL.GetDataToList(DBCR).Count == 0)
                {
                    Console.WriteLine();
                    Console.WriteLine("".PadRight(HeaderPadding, '='));
                    Console.WriteLine();
                    Console.WriteLine("The required dataset does not exist. Run option #5 from main menu to create the needed dataset");
                    Console.WriteLine("Press <ENTER> to go back to main menu");
                    Console.WriteLine();
                    Console.WriteLine("".PadRight(HeaderPadding, '='));
                    Console.ReadLine();
                    CreateUserMenu();
                }

                Console.WriteLine();
                Console.WriteLine("".PadRight(HeaderPadding, '='));
                Console.WriteLine();
                Console.WriteLine("Press <ENTER> to update an existing Customer");
                Console.WriteLine();
                Console.WriteLine("".PadRight(HeaderPadding, '='));
                Console.ReadLine();

                DBCR.SQLString = @"UPDATE Customer 
                    SET FirstName = 'John', Email = 'john.doe@anywhere.us'	 
	                WHERE FirstName = 'Jon'
	                AND LastName = 'Doe';";
                if (MSSQL.SendSQLCommand(DBCR))
                {
                    Console.WriteLine("Jon Doe dataset(s) was successfully updated to John Doe!");
                }
                else
                {
                    Console.WriteLine("command not executed successfully");
                    if (MSSQL.boError)
                    {
                        Console.WriteLine("Please ask your Support for help and please provide the following information");
                        Console.WriteLine("");
                        Console.WriteLine($"   Error Number: <{MSSQL.intErrorNumber}>, error Description: <{MSSQL.strgErrorDescription}>");
                    }
                }
                Console.WriteLine("");
                Console.WriteLine("Press <ENTER> to to view the data just updated!");
                Console.ReadLine();
                DBCR.SQLString = @"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email
                       FROM Customer 
                       WHERE FirstName = 'John' AND LastName = 'Doe';";
                DBCR.Table = "Customer";
                DBCR.Remark = @"  Firstly we check if John Doe exists 
  The output based on: " + DBCR.SQLString;
                GenerateOutput(DBCR, MSSQL.GetHeaderToList(DBCR), MSSQL.GetDataToList(DBCR), 2, true);
                // 
                Console.WriteLine();
                Console.WriteLine("".PadRight(HeaderPadding, '='));
                Console.WriteLine();
                Console.WriteLine("Press <ENTER> to get back to the main menu");
                Console.WriteLine();
                Console.WriteLine("".PadRight(HeaderPadding, '='));
                Console.ReadLine();
                CreateUserMenu();
            }
            catch (Exception EX)
            {
                Console.WriteLine(EX.Message);
            }

        }


        // Customer Requirements #7
        public void GetCustomerCountByCountry()
        {
            
            try
            {
                // we use the following SQL Statement to generate the required data and output as an example
                DBCR.SQLString = @"SELECT Country, COUNT(CustomerId) Count 
                       FROM Customer 
                       GROUP BY Country 
                       ORDER BY Count DESC";
                DBCR.Table = "Customer";
                DBCR.Remark = "       Used statement: " + DBCR.SQLString;
                GenerateOutput(DBCR, MSSQL.GetHeaderToList(DBCR), MSSQL.GetDataToList(DBCR), 2);
                Console.WriteLine();
                Console.WriteLine("".PadRight(HeaderPadding, '='));
                Console.WriteLine();
                Console.WriteLine("Press <ENTER> to get back to the main menu");
                Console.WriteLine();
                Console.WriteLine("".PadRight(HeaderPadding, '='));
                Console.ReadLine();

                CreateUserMenu();
            }
            catch (Exception EX)
            {
                Console.WriteLine(EX.Message);
            }

        }


        // Customer Requirements #8
        public void GetHighestSpenders()
        {
            try
            {
                // we use the following SQL Statement to generate the required data and output as an example
                DBCR.SQLString = @"SELECT Customer.FirstName, Customer.LastName, SUM(Invoice.Total) 'Sum' 
                       FROM Invoice 
                       INNER JOIN Customer ON Invoice.CustomerId = Customer.CustomerId 
                       GROUP BY Invoice.CustomerId, Customer.FirstName, Customer.LastName 
                       ORDER BY Sum DESC;";
                DBCR.Table = "Customer";
                DBCR.Remark = "       Used statement: " + DBCR.SQLString;
                GenerateOutput(DBCR, MSSQL.GetHeaderToList(DBCR), MSSQL.GetDataToList(DBCR), 2);
                // 
                Console.WriteLine();
                Console.WriteLine("".PadRight(HeaderPadding, '='));
                Console.WriteLine();
                Console.WriteLine("Press <ENTER> to get back to the main menu");
                Console.WriteLine();
                Console.WriteLine("".PadRight(HeaderPadding, '='));
                Console.ReadLine();

                CreateUserMenu();
            }
            catch (Exception EX)
            {
                Console.WriteLine(EX.Message);
            }

        }


        // For a given Customer most popular Genre #9
        public void MostPopularGenre(int CustomerID,bool skip = false)
        {
            List<string> RetString = new List<string>();
            try
            {
                // we use the following SQL Statement to generate the required data and output as an example
                DBCR.SQLString = @$"SELECT TOP(2) GE.Name AS Name, COUNT(*) AS AmountListen
                       FROM Invoice IV
                       LEFT JOIN InvoiceLine IL ON IV.InvoiceId = IL.InvoiceId
                       LEFT JOIN Track TR ON IL.TrackId = TR.TrackId
                       LEFT JOIN Genre GE ON TR.GenreId = GE.GenreId
                       WHERE CustomerId = {CustomerID}
                       GROUP BY GE.Name
                       ORDER BY AmountListen DESC;";
                DBCR.Table = "Genre";
                DBCR.Remark = "       Used statement: " + DBCR.SQLString;

                if (MSSQL.GetDataToList(DBCR).Count == 2 )
                {
                    RetString = MSSQL.GetDataToList(DBCR);

                    if (RetString[0].Substring(RetString[0].IndexOf("|") + 1) == RetString[1].Substring(RetString[1].IndexOf("|") + 1))
                    {
                        GenerateOutput(DBCR, MSSQL.GetHeaderToList(DBCR), MSSQL.GetDataToList(DBCR), 2);
                    } 
                    else
                    {
                        RetString.RemoveAt(1);
                        GenerateOutput(DBCR, MSSQL.GetHeaderToList(DBCR), RetString, 2);
                    }
                }
                else
                {
                    GenerateOutput(DBCR, MSSQL.GetHeaderToList(DBCR), MSSQL.GetDataToList(DBCR), 2);
                }

                    Console.WriteLine();
                    Console.WriteLine("".PadRight(HeaderPadding, '='));
                    Console.WriteLine();
                    if (skip == false)
                    {
                        Console.WriteLine("Press <ENTER> to show next example");
                    }
                    else
                    {
                        Console.WriteLine("Press <ENTER> to get back to the main menu");
                    }
                    Console.WriteLine();
                    Console.WriteLine("".PadRight(HeaderPadding, '='));
                    Console.ReadLine();
                    if (skip == false) MostPopularGenre(56, true);
                    CreateUserMenu();

                // 

                

            }
            catch (Exception EX)
            {
                Console.WriteLine(EX.Message);
            }

        }

        // Generate Ouput
        public void GenerateOutput(ClsMSSQL.DBCredentials DBCRC, List<string> ColumnNames, List<string> RowsByColumn, int intAdditionalSpace, bool suppresScreenClear = false) 
        {
            string strTEMP; int intTemp; string[] strSplit;
            Dictionary<string,int> ColumnNameAndWidth = new Dictionary<string,int>();
            // get maximum width of each used Column by using Column names into a Dictionary for later use
            foreach (string ColumnName in ColumnNames)
            {
                // determine now the maximum column width for each column
                DBCRC.SQLString = $"SELECT {ColumnName} FROM " + DBCRC.Table;
                
                intTemp = MSSQL.GetMaxWidthOf(DBCRC);
                // check if the lenght of the Column Header Name is longer than the determine maximum column width
                // if yes than take the lenght instead of the determine maximum column width
                if (ColumnName.Length > intTemp) intTemp = ColumnName.Length;
                // Creat dictionary entry, key is the Column Name, Value is the maximum width
                ColumnNameAndWidth.Add(ColumnName, intTemp);
            }

            // create header 
            if (suppresScreenClear == false) Console.Clear();
            Console.WriteLine("".PadRight(HeaderPadding, '='));
            Console.WriteLine();
            Console.WriteLine("  Below you will find your requested data!");
            if (DBCRC.Remark.Length != 0)
            {
                Console.WriteLine();
                Console.BackgroundColor = ConsoleColor.Blue;
                Console.ForegroundColor = ConsoleColor.White;
                //Console.WriteLine(DBCRC.Remark);
                Console.ResetColor();
                ColorMyOutput(DBCRC.Remark);
                
            }
            Console.WriteLine();
            Console.WriteLine("".PadRight(HeaderPadding, '='));
            Console.WriteLine();

            // create Data header by using prior generated Colum width
            strTEMP = "";
            foreach (KeyValuePair<string,int> Column in ColumnNameAndWidth)
            {
                // run throu all dictionary antries and generate a header string for output
                strTEMP = strTEMP + Column.Key.ToString().PadRight(Column.Value + intAdditionalSpace, ' ') + "|";
            }
            Console.WriteLine(strTEMP);

            // generate ???
            strTEMP = "";
            foreach (KeyValuePair<string, int> Column in ColumnNameAndWidth)
            {
                // run throu all dictionary antries and generate a header string for output
                strTEMP = strTEMP + "".PadRight(Column.Value + intAdditionalSpace, '-') + "+";
            }
            Console.WriteLine(strTEMP);

            foreach (string eachLine in RowsByColumn)
            {
                strSplit = eachLine.Split("|");
                strTEMP = "";
                for (int intCount = 0; intCount < strSplit.Length; intCount++)
                {
                    strTEMP = strTEMP + strSplit[intCount].PadRight(ColumnNameAndWidth[ColumnNames[intCount]] + intAdditionalSpace, ' ') + "|";
                }
                Console.WriteLine(strTEMP);
            }
        }


        // Colore my Output
        public void ColorMyOutput(string strText)
        {
            // Colorize given text like a SQL intellisense
            var words = Regex.Split(strText, @"( )");

            foreach (var word in words)
            {
                switch (word)
                {
                    case "SELECT":
                    case "FROM":
                    case "WHERE":
                    case "ON":
                    case "AND":
                    case "OR":
                    case "TOP":
                    case "AS":
                    case "BY":
                    case "GROUP":
                    case "ORDER":
                    case "DESC":
                    case "ASC":
                    case "ROWS":
                    case "FETCH":
                    case "ONLY":
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.Write(word);
                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                    case "OFFSET":
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.Write(word);
                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                    case "NEXT":

                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.Write(word);
                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                    case "LEFT":
                    case "RIGHT":
                    case "INNER":
                    case "JOIN": //case string TMP when TMP.Contains("("):
                        Console.ForegroundColor = ConsoleColor.DarkGray;
                        Console.Write(word);
                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                    case string TMP when TMP.Contains("COUNT"):
                        Console.ForegroundColor = ConsoleColor.Magenta;
                        Console.Write(word.Substring(0, 5));
                        Console.ForegroundColor = ConsoleColor.DarkGray;
                        Console.Write(word.Substring(5));
                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                    case string TMP when TMP.Contains("SUM"):
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write(word.Substring(0, 3));
                        Console.ForegroundColor = ConsoleColor.DarkGray;
                        Console.Write(word.Substring(3));
                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                    //case "":
                    //break;
                    case string TMP when TMP.Contains("'"):
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write(word);
                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                    default:
                        Console.Write(word);
                        break;
                }

            }
        }

        /// <summary>
        /// Building our user menu
        /// </summary>
        public void CreateUserMenu()
        {
            Console.Clear();
            Console.WriteLine("".PadRight(HeaderPadding, '='));
            Console.WriteLine();
            Console.WriteLine("  Welcome to the user menu !");
            Console.WriteLine();
            Console.WriteLine("".PadRight(HeaderPadding, '='));
            Console.WriteLine();
            Console.WriteLine("    1. Show all customers from our database");
            Console.WriteLine("    2. Show a specific customer from our database by Id (11)");
            Console.WriteLine("    3. Show a specific customer from our database by Name (Victor Stevens)");
            Console.WriteLine("    4. Show all customers data page by page");
            Console.WriteLine("    5. Add a new customer to the database");
            Console.WriteLine("    6. Update an existing customer");
            Console.WriteLine("    7. Show the ammount of our customers in each country (ordered high -> low)");
            Console.WriteLine("    8. Show customers who are the highest spenders (ordered high -> low)");
            Console.WriteLine("    9. Show the most popular genre for given customers 8 & 56 to show the differences");
            Console.WriteLine("    0. Exit application");
            Console.WriteLine();
            Console.WriteLine("".PadRight(HeaderPadding, '='));
            Console.WriteLine();
            Console.WriteLine("  Please select one of the provided options:");            
            Console.WriteLine();
            Console.WriteLine("".PadRight(HeaderPadding, '='));
            
            ConsoleKeyInfo input;
            input = Console.ReadKey();            
            
            switch (input.Key.ToString())
            {
                case "D1":
                    GetCustomerFromDB();
                    break;

                case "D2":
                    GetSpecificCustomerFromDB(@"
                       WHERE CustomerId = 11");
                    break;

                case "D3":
                    GetSpecificCustomerFromDB(@"
                       WHERE LastName = 'Stevens' AND FirstName = 'Victor'");
                    break;

                case "D4":
                    GetCustomerFromDBbyPage(0, 20);
                    break;

                case "D5":
                    AddNewCustomer();
                    break;

                case "D6":
                    UpdateExistingCustomer();
                    break;

                case "D7":
                    GetCustomerCountByCountry();
                    break;

                case "D8":
                    GetHighestSpenders();
                    break;

                case "D9":
                    MostPopularGenre(8);
                    break;

                case "D0":
                    Environment.Exit(0);
                    break;

                default:
                    //Console.Clear();                    
                    Console.WriteLine();
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("Please select only one of the available options");
                    Console.ResetColor();
                    Thread.Sleep(1500);
                    
                    CreateUserMenu();
                    break;
                       
                   

            }
        }

        public void setEnvironment()
        {
            // prepare structure to hold the needed information for a later use in the Stringbuilder for example
            DBCR.ServerName = "Localhost";
            DBCR.Database = "Chinook";
            DBCR.User = "";
            DBCR.Passw = "";
            DBCR.port = "";
            DBCR.Table = "";
            DBCR.SQLString = "";
            DBCR.Remark = "";

            // set Console window style
            Console.CursorVisible = false;
            Console.SetWindowSize(Console.LargestWindowWidth, Console.LargestWindowHeight);
            Console.Title = "Assignment 2: Data Persistence and Access";
        }

        static void Main(string[] args)
        {
            // set all for the environment needed things
            Program myProgram = new Program();
            myProgram.setEnvironment();

            Console.WriteLine("Try to connect Server: " + myProgram.DBCR.ServerName);
            try
            {
                if (myProgram.MSSQL.CheckServer(myProgram.DBCR))
                {
                    Console.WriteLine("Connected successfully jump to Menu");
                    myProgram.CreateUserMenu();
                } else
                {
                    Console.WriteLine($"unable to connect to Database: <{myProgram.DBCR.Database}> on server: " + myProgram.DBCR.ServerName);
                    Console.WriteLine("Press enter to close Program");
                    Console.ReadLine();
                }
            }
            catch (Exception EX)
            {
                Console.WriteLine("Error: " + EX.Message);
                Console.WriteLine("Press enter to close Program");
                Console.ReadLine();
            }

            
        }
    }
}
