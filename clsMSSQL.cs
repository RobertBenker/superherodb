﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Common;
using Microsoft.Data.SqlClient;

namespace dotnetcore
{
    class ClsMSSQL
    {
            public int intErrorNumber;
            public string strgErrorDescription;
            public bool boError;

        public struct DBCredentials
        {
            public string ServerName;
            public string Database;
            public string User; // If needed otherwise set to empty like = ""
            public string Passw; // If DBUser is not given this will be ignored
            public string port;
            public string Table;
            public string SQLString;
            public string Remark;
        }

        static string GenSQLConnectionString(DBCredentials DBCR)
        {
            // this will create the SQLConnectionString based on the given set of Parameter
            SqlConnectionStringBuilder sqlCONNString = new SqlConnectionStringBuilder();

            sqlCONNString.InitialCatalog = DBCR.Database;

            if (DBCR.port == "")
            {
                // If no port name is given use only the blank server name
                sqlCONNString.DataSource = DBCR.ServerName;
            } else
            {
                // otherwise add the given port to the server string
                sqlCONNString.DataSource = DBCR.ServerName + ":" + DBCR.port;
            }

            if (DBCR.User.Length == 0)
            {
                // If no user is given use the windows internal credentials
                sqlCONNString.IntegratedSecurity = true;
                sqlCONNString.TrustServerCertificate = true;
            } else
            {
                // Otherwise use the given user Cred
                sqlCONNString.UserID = DBCR.User;
                sqlCONNString.Password = DBCR.Passw;
                sqlCONNString.IntegratedSecurity = false;
                sqlCONNString.TrustServerCertificate = true;
            }
            return sqlCONNString.ConnectionString;
        }

        public  bool CheckServer(DBCredentials DBCR)
        {
            SqlConnection sqlCONN = new SqlConnection(GenSQLConnectionString(DBCR));

            try
            {
                sqlCONN.Open(); // open and close connection immediately to check if the given parameter are correct
                sqlCONN.Close(); // and the server is up an running
                return true;
            }
            catch (Exception EX)
            {
                intErrorNumber = 99;
                strgErrorDescription = "InternalError in clsMSSQL.CheckServer: " + EX.Message;
                boError = true;
                return false;
            }
        }

        public List<string> GetHeaderToList(DBCredentials DBCR)
        {
            List<string> RetString = new List<string>();
            SqlConnection sqlCONN = new SqlConnection(GenSQLConnectionString(DBCR));

            RetString.Clear();

            try
            {
                sqlCONN.Open();
                using (SqlCommand sqlCMD = new SqlCommand(DBCR.SQLString, sqlCONN))
                {
                    using (SqlDataReader sqlReader = sqlCMD.ExecuteReader())
                    {
                            for (int Column = 0; Column < sqlReader.FieldCount; Column++)
                            {
                            RetString.Add(sqlReader.GetName(Column));
                            }
                    }
                }
                sqlCONN.Close();
            }
            catch (Exception EX)
            {
                intErrorNumber = 99;
                strgErrorDescription = "InternalError in clsMSSQL.GetHeaderToList: " + EX.Message;
                boError = true;
                RetString.Clear();
            }
            return RetString;
        }

        public List<string> GetDataToList(DBCredentials DBCR)
        {
            string tmpString = "";
            List<string> RetString = new List<string>();
            SqlConnection sqlCONN = new SqlConnection(GenSQLConnectionString(DBCR));

            RetString.Clear();

            try
            {
                sqlCONN.Open();
                using (SqlCommand sqlCMD = new SqlCommand(DBCR.SQLString,sqlCONN))
                {
                    using (SqlDataReader sqlReader = sqlCMD.ExecuteReader())
                    {
                            while (sqlReader.Read())
                            {
                                tmpString = "";
                                for (int Column = 0; Column < sqlReader.FieldCount; Column++)
                                {
                                    tmpString = tmpString + sqlReader.GetValue(Column) + "|";
                                }
                                RetString.Add(tmpString.Substring(0, tmpString.Length - 1));
                            }
                    }
                }
                sqlCONN.Close();
             }
            catch (Exception EX)
            {
                intErrorNumber = 99;
                strgErrorDescription = "InternalError in clsMSSQL.GetDataToList: " + EX.Message;
                boError = true;
                RetString.Clear();

            }
            return RetString;
        }

        public int GetMaxWidthOf(DBCredentials DBCR)
        {
            int RC = -1;
            List<string> RetString = new List<string>();
            SqlConnection sqlCONN = new SqlConnection(GenSQLConnectionString(DBCR));
            try
            {
                sqlCONN.Open();
                using (SqlCommand sqlCMD = new SqlCommand(DBCR.SQLString, sqlCONN))
                {
                    using (SqlDataReader sqlReader = sqlCMD.ExecuteReader())
                    {
                        while (sqlReader.Read())
                        {
                            if (sqlReader.GetValue(0).ToString().Length > RC) RC = sqlReader.GetValue(0).ToString().Length;
                        }
                    }
                }
                sqlCONN.Close();
                return RC;
            }
            catch (Exception EX)
            {
                intErrorNumber = 99;
                strgErrorDescription = "InternalError in clsMSSQL.GetMaxWidthOf: " + EX.Message;
                boError = true;
                return -1;
            }
        }

        public bool SendSQLCommand(DBCredentials DBCR)
        {
            SqlConnection sqlCONN = new SqlConnection(GenSQLConnectionString(DBCR));
            try
            {
                sqlCONN.Open();
                using (SqlCommand sqlCMD = new SqlCommand(DBCR.SQLString, sqlCONN))
                {
                    sqlCMD.ExecuteReader();
                }
                sqlCONN.Close();
                return true;
            }
            catch (Exception EX)
            {
                boError = true;
                intErrorNumber = 99;
                strgErrorDescription = "InternalError in clsMSSQL.SendSQLCommand: " + EX.Message;
                return false;
            }
        }
    }
}
